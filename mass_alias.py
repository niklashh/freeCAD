import FreeCAD, re

sss = App.ActiveDocument.findObjects("Spreadsheet::Sheet")
for ss in sss:
	print('Running mass_alias on `%s`' % ss.Label)
	row = 1
	lastn = None
	while 1:
		try:
			name = ss.get('A%d' % row).encode('utf-8')
			value = ss.get('B%d' % row)
		except ValueError:
			if lastn == None:
				print('Done, rows: %d' % (row - 1))
				break
			else:
				lastn = None
				continue
		findall = re.findall('^#(.*)$', name)
	#	print(name, value, findall)
	
		if len(findall) and isinstance(value,float):
			alias = findall[0]
			
			print('Aliasing {} @ B{} as "{}"'.format(value,row,alias))
			try:
				ss.setAlias('B%d' % row, alias)
			except Exception as e:
				print('{}'.format(e))
	
		lastn = name
		row += 1