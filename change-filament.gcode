{if layer_z < max_print_height}G1 Z{z_offset+min(layer_z+30, max_print_height)}{endif} ; Move print head up
G1 Y0
M0 ; pause
G91 ; use relative coordinates
G1 E20 ; prime nozzle after color change
G90 ; use absolute coordinates
